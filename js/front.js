
let front = (function () {

    let construct = function (baseUri) {
        $('select').formSelect();
        productos.init(baseUri);
    };

    return {
        construct: construct,
    };
})();

let productos = (function () {
    let urlAjax;

    let init = function (baseUri) {
        urlAjax = baseUri + '/ajaxProductos.php';
        //console.log(urlAjax);
        let ajax = new Remote(urlAjax, 'GET', onSuccess);
        ajax.request();
    };

    let refreshTabla = function (pagina) {
        let ajax = new Remote(urlAjax, 'GET', onSuccess);
        ajax.setDataObject({
            busqueda: busqueda.getVal(),
            pagina: pagina,
        });
        ajax.request();
    };

    let onSuccess = function (data) {
        tabla.replace(data.tabla);
        paginador.replace(data.paginador);
        configBtnsPaginador();
    };

    let onBusquedaChanged = function () {
        refreshTabla(1);
        console.log('onBusquedaChanged');
    };

    let configBtnsPaginador = function () {
        $('.page-item').click(onClickPageItem);
    };

    let onClickPageItem = function (event) {
        event.preventDefault();
        refreshTabla($(this).data('id'));
        console.log($(this).data('id'));
    };

    let paginador = new Container('paginador');
    let tabla = new Container('tabla');
    let busqueda = new InputText('busqueda', null, onBusquedaChanged);

    return {
        init: init,
    };
})();
