<?php

const BASE_PATH = __DIR__;

spl_autoload_register( function($className) {
    $subPath = str_replace("\\",'/',$className);
    $filepath = __DIR__ . "/{$subPath}.php";
    if (file_exists($filepath)) {
        include_once $filepath;
    }
} );

$baseUri = '/tg/clase6';
session_start();


