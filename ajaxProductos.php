<?php

use Tg\BasicCrud\Criteria;

require_once 'autoload.php';

$busqueda = isset($_GET['busqueda']) ? $_GET['busqueda'] : '';
$pagina = isset($_GET['pagina']) ? (int) $_GET['pagina'] : 1;
$criteria = new Criteria($busqueda, $pagina);
$response = $criteria->ajaxResponse();

echo json_encode($response->toArray());
