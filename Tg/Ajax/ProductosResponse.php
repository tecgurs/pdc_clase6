<?php
namespace Tg\Ajax;


class ProductosResponse extends BasicResponse
{
    /** @var string */
    private $paginador;
    /** @var string */
    private $tabla;

    public function __construct(bool $success, string $message, string $paginador = '', string $tabla = '')
    {
        parent::__construct($success, $message);
        $this->paginador = $paginador;
        $this->tabla = $tabla;
    }

    public function toArray(): array
    {
        return array_merge(parent::toArray(), [
            'paginador' => $this->paginador,
            'tabla' => $this->tabla,
        ]) ;
    }
}
