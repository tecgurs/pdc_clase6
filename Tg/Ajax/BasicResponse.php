<?php
/**
 * @Filename: BasicResponse.php
 * @Description:
 * @CreatedAt: 09/09/19 19:40
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Ajax;


class BasicResponse
{
    /** @var bool */
    private $success;
    /** @var string */
    private $message;

    /**
     * BasicResponse constructor.
     * @param bool $success
     * @param string $message
     */
    public function __construct(bool $success, string $message)
    {
        $this->success = $success;
        $this->message = $message;
    }

    public function toArray(): array
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
        ];
    }
}
