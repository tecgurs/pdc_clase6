<?php
/**
 * @Filename: Auth.php
 * @Description:
 * @CreatedAt: 09/09/19 20:58
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Acl;


use Tg\Factory;

class Auth
{
    public function check(string $user, string $password): bool
    {
        $mysql = Factory::mysql();
        $consulta = "SELECT email, nombre FROM usuarios WHERE email=:email AND password=MD5(:password)";
        $statement = $mysql->prepare($consulta);
        $statement->execute([
            'email' => $user,
            'password' => $password,
        ]);
        $resultado = $statement->fetch();
        if ($resultado == false) {
            return false;
        }

        $_SESSION['login'] = true;
        $_SESSION['email'] = $resultado['email'];
        $_SESSION['nombre'] = $resultado['nombre'];

        return true;
    }
}
