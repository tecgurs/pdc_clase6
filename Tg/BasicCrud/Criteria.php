<?php
namespace Tg\BasicCrud;


use Tg\Ajax\ProductosResponse;
use Tg\Db\Exception as DbException;
use Tg\Factory;

class Criteria
{
    const ITEMS_X_PAG = 5;

    /** @var string */
    public $busqueda;
    /** @var int */
    public $pagina;

    /** @var int */
    private $offset;
    /** @var int */
    private $totalItems;

    /**
     * Criteria constructor.
     * @param string $busqueda
     * @param int $pagina
     */
    public function __construct(string $busqueda = '', int $pagina = 1)
    {
        $this->busqueda = $busqueda;
        $this->pagina =  $pagina;
        if ($pagina < 1) {
            $this->pagina =  1;
        }
    }

    /**
     * @return ProductosResponse
     * @throws DbException
     */
    public function ajaxResponse(): ProductosResponse
    {
        $arrayProductos = $this->readFromDb();

        $paginador = $this->generatePaginador();

        $tabla = new TablaProductos();

        return new ProductosResponse(true, 'Success', $paginador, $tabla->fromArrayProductos($arrayProductos));
    }

    private function generatePaginador(): string
    {
        $totPaginas = ceil($this->totalItems / self::ITEMS_X_PAG);

        $htmlItems = '';
        for ($x = 1; $x <= $totPaginas; $x++) {
            if ($x == $this->pagina) {
                $htmlClass = 'active';
            } else {
                $htmlClass = 'waves-effect';
            }
            $htmlItems .= <<<html
<li class="{$htmlClass}"><a href="#" class="page-item" data-id="{$x}">{$x}</a></li>
html;
        }

        $inicio = $this->offset + 1;
        $fin = min($inicio + self::ITEMS_X_PAG - 1, $this->totalItems);

        return <<<html
<ul class="pagination">
{$htmlItems}
</ul>
Mostrando {$inicio} - {$fin} de {$this->totalItems} resultados
html;
    }

    /**
     * @return array
     * @throws DbException
     */
    private function readFromDb(): array
    {
        $mysql = Factory::mysql();

        $where1 = empty($this->busqueda) ? '' : 'AND p.caption LIKE :busqueda';

        $consulta1 = <<<sql
SELECT p.id, p.code, p.caption, p.precio_centavos, u.caption AS uCaption
FROM productos AS p
INNER JOIN unidades AS u ON p.unidadId = u.id
WHERE 1
{$where1}
sql;
        $statement1 = $mysql->prepare($consulta1);
        $statement1->execute([
            'busqueda' => "%{$this->busqueda}%",
        ]);

        $this->totalItems = count($statement1->fetchAll());

        $this->offset = ($this->pagina - 1) * self::ITEMS_X_PAG;
        $items_x_pag = self::ITEMS_X_PAG;

        $consulta2 = <<<sql
SELECT p.id, p.code, p.caption, p.precio_centavos, u.caption AS uCaption
FROM productos AS p
INNER JOIN unidades AS u ON p.unidadId = u.id
WHERE 1
{$where1}
LIMIT {$this->offset}, {$items_x_pag}
sql;

        $statement2 = $mysql->prepare($consulta2);
        $statement2->execute([
            'busqueda' => "%{$this->busqueda}%",
        ]);

        return $statement2->fetchAll();

    }
}
