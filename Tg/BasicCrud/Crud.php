<?php

namespace Tg\BasicCrud;


use Tg\Db\Exception as DbException;
use Tg\Db\Mysql;
use Tg\Factory;
use Tg\Money;

class Crud
{
    /** @var string */
    private $tipo = '';
    /** @var string */
    private $message;
    /** @var bool */
    private $resultado;
    /** @var Defaults */
    private $defaults;

    /** @var Mysql */
    private $mysql;

    /**
     * Crud constructor.
     * @throws Exception
     * @throws DbException
     */
    public function __construct()
    {
        $this->defaults = new Defaults();
        if (isset($_GET['borrar'])) {
            $this->tipo = 'borrar';
            $this->manejaBorrar();
            return;
        }

        if (isset($_GET['editar'])) {
            $this->tipo = 'editar';
            $this->manejaEditar();
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
           if ($_POST['id'] != 0) {
               //var_dump($_POST); exit;
               $this->tipo = 'botonEditar';
               $this->manejaBotonEditar();
               return;
           }
           $this->tipo = 'crear';
           $this->manejaCrear();
           return;
        }

    }

    /**
     * @return string
     */
    public function getTipo(): string
    {
        return $this->tipo;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return bool
     */
    public function getResultado(): bool
    {
        return $this->resultado;
    }

    /**
     * @return Defaults
     */
    public function getDefaults(): Defaults
    {
        return $this->defaults;
    }

    public function generateBotonCaption(): string
    {
        if ($this->tipo == 'editar' || $this->tipo == 'botonEditar') {
            return 'Editar';
        }
        return 'Crear';
    }

    /**
     * @throws Exception
     * @throws DbException
     */
    private function manejaBorrar()
    {
        $productoId = (int) $_GET['borrar'];
        if ($productoId < 1) {
            throw new Exception('El id es incorrecto');
        }
        try {
            /*$producto =*/ $this->requireProducto($productoId);
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            $this->resultado = false;
            return;
        }

        $consulta = "DELETE FROM productos WHERE id=:id";
        $statement = $this->requireMysql()->prepare($consulta);
        $statement->execute(['id' => $productoId]);
        $this->resultado = true;
    }

    /**
     * @throws Exception
     * @throws DbException
     */
    private function manejaEditar()
    {
        $productoId = (int) $_GET['editar'];
        if ($productoId < 1) {
            throw new Exception('El id es incorrecto');
        }
        try {
            $producto = $this->requireProducto($productoId);
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            $this->resultado = false;
            return;
        }
        $unidades = [
            1 => 'kilo',
            2 => 'pieza',
        ];
        $precioMoney = Money::fromCentavos($producto['precio_centavos']);
        $this->defaults = new Defaults(
            $producto['id'],
            $producto['caption'],
            $producto['code'],
            $precioMoney->getFloat(),
            $unidades[(int) $producto['unidadId']]
        );
        $this->resultado = true;
    }

    /**
     * @param int $productoId
     * @return array
     * @throws Exception
     * @throws DbException
     */
    private function requireProducto(int $productoId): array
    {
        //$mysql = Factory::mysql();
        $consulta = "SELECT * FROM productos WHERE id=:id";
        $statement = $this->requireMysql()->prepare($consulta);
        $statement->execute(['id' => $productoId]);
        $resultado = $statement->fetch();

        if ($resultado == false) {
            throw new Exception("No existe el producto con id {$productoId}");
        }

        return $resultado;
    }

    /**
     * @throws DbException
     */
    private function manejaBotonEditar()
    {
        $this->defaults = new Defaults(
            $_POST['id'],
            $_POST['nombre'],
            $_POST['code'],
            $_POST['precio'],
            $_POST['unidad']
        );

        try {
            /*$producto =*/ $this->requireProducto($_POST['id']);
        } catch (Exception $e) {
            $this->message = $e->getMessage();
            $this->resultado = false;
            return;
        }

        $consulta = <<<sql
UPDATE productos
SET mtime = NOW(),
    code = :code,
    caption = :caption,
    precio_centavos = :precio,
    unidadId = :unidadId
WHERE id = :id
sql;
        $statement = $this->requireMysql()->prepare($consulta);
        $unidades = ['kilo' => 1, 'pieza' => 2];
        $precioMoney = Money::fromFloat($_POST['precio']);
        $statement->execute([
            'code' => $_POST['code'],
            'caption' => $_POST['nombre'],
            'precio' => $precioMoney->getCentavos(),
            'unidadId' => $unidades[$_POST['unidad']],
            'id' => $_POST['id'],
        ]);
        $this->resultado = true;
    }

    /**
     * @throws DbException
     */
    private function manejaCrear()
    {
        $this->defaults = new Defaults(
            0,
            $_POST['nombre'],
            $_POST['code'],
            $_POST['precio'],
            $_POST['unidad']
        );

        if ($_FILES['foto']['size'] == 0) {
            $this->resultado = false;
            $this->message = 'Debes subir un archivo';
            return;
        }

        $mime_type = $_FILES['foto']['type'];
        if ($mime_type != 'image/jpeg') {
            $this->resultado = false;
            $this->message = 'Debes subir un archivo JPG';
            return;
        }

        $code = $_POST['code'];
        if (empty($code)) {
            $this->resultado = false;
            $this->message = 'El campo code es mandatorio';
            return;
        }

        $tmp_name = $_FILES['foto']['tmp_name'];
        $dir = BASE_PATH . '/img/';
        $filename = $dir . $code . '.jpg';

        move_uploaded_file($tmp_name, $filename);

        //var_dump($filename); exit;



        //var_dump($_FILES); exit;

        $consulta = <<<sql
INSERT INTO productos
(ctime, code, caption, precio_centavos, unidadId)
VALUES
(NOW(), :code, :caption, :precio, :unidadId)
sql;
        $statement = $this->requireMysql()->prepare($consulta);
        $unidades = ['kilo' => 1, 'pieza' => 2];
        $precioMoney = Money::fromFloat($_POST['precio']);
        $statement->execute([
            'code' => $code,
            'caption' => $_POST['nombre'],
            'precio' => $precioMoney->getCentavos(),
            'unidadId' => $unidades[$_POST['unidad']],
        ]);
        $this->resultado = true;
    }

    private function requireMysql(): Mysql
    {
        if (empty($this->mysql)) {
            $this->mysql = Factory::mysql();
        }

        return $this->mysql;
    }
}
