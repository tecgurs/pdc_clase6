<?php
/**
 * @Filename: TablaProductos.php
 * @Description:
 * @CreatedAt: 17/09/19 17:52
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\BasicCrud;


use Tg\Db\Exception as DbException;
use Tg\Factory;
use Tg\Money;

class TablaProductos
{

    public function fromArrayProductos(array $productos): string
    {
        return $this->generateTabla($productos);
    }

    /**
     * @return string
     */
    public function toHtml(): string
    {
        try {
            $filas = $this->readFromDb();
            return $this->generateTabla($filas);
        } catch (DbException $e) {
            return 'No hay resultados';
        }
    }

    /**
     * @return array
     * @throws DbException
     */
    private function readFromDb(): array
    {
        $mysql = Factory::mysql();
        $consulta = <<<sql
SELECT p.id, p.code, p.caption, p.precio_centavos, u.caption AS uCaption
FROM productos AS p
INNER JOIN unidades AS u ON p.unidadId = u.id
sql;
        $statement = $mysql->query($consulta);

        return $statement->fetchAll();
    }

    private function generateTabla(array $filas): string
    {
        $htmlTabla = <<<html
<tr>
<th>ID</th>
<th>Code</th>
<th>Producto</th>
<th>Precio</th>
<th>Unidad</th>
<th>Foto</th>
<th>Acciones</th>
</tr>
html;

        foreach ($filas as $fila) {
            $htmlTabla .= $this->generateFila($fila);
        }

        return <<<html
<table>
{$htmlTabla}
</table>
html;

    }

    private function generateFila(array $fila): string
    {
        $precioMoney = Money::fromCentavos($fila['precio_centavos']);

        $htmlFila = <<<html
<tr>
<td>{$fila['id']}</td>
<td>{$fila['code']}</td>
<td>{$fila['caption']}</td>
<td>{$precioMoney->getString('$ ')}</td>
<td>{$fila['uCaption']}</td>
<td><img height="50px" src="/tg/clase6/img/{$fila['code']}.jpg" alt="{$fila['uCaption']}"></td>
<td>
<a href="/tg/clase6/index.php?borrar={$fila['id']}">Borrar</a> | 
<a href="/tg/clase6/index.php?editar={$fila['id']}">Editar</a>
</td>
</tr>
html;

        return $htmlFila;
    }
}
