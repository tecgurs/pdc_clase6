<?php
namespace Tg\Tienda\Models;


use stdClass;
use Tg\Db\Model;

class Unidad extends Model
{
    protected static $tableName = 'unidades';

    /** @var string */
    protected $caption;
    /** @var string */
    protected $caption_plural;

    protected $fields = ['caption', 'caption_plural'];

    /**
     * Unidad constructor.
     * @param string $caption
     * @param string $caption_plural
     */
    public function __construct(string $caption, string $caption_plural)
    {
        $this->caption = $caption;
        $this->caption_plural = $caption_plural;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return string
     */
    public function getCaptionPlural(): string
    {
        return $this->caption_plural;
    }

    protected static function readFromStdClass(stdClass $object): Unidad
    {
        $unidad = new self(
            $object->caption,
            $object->caption_plural
        );
        $unidad->setFromStdClass($object);

        return $unidad;
    }
}
