<?php

namespace Tg\Tienda\Articulos;


use Tg\Db\Exception;
use Tg\Tienda\Models\Articulos;
use Tg\Tienda\Productos\Item as ProductosItem;

class Item
{
    /** @var int */
    private $id;
    /** @var ProductosItem */
    private $productoItem;
    /** @var int */
    private $cantidad;

    private function __construct()
    {
    }

    public function generateHtmlArticulo(): string
    {
        $precioMoney = $this->productoItem->getPrecio();

        return <<<html
<div class="card horizontal">
          <div class="card-image">
            {$this->productoItem->generateImgTag()}
            
          </div>
          <div class="card-stacked">
            <div class="card-content">
              <p>{$this->cantidad} artículos @ {$precioMoney->getString('$ ')}</p>
              <p><strong>Total {$precioMoney->getString('$ ', $this->cantidad)}</strong></p>
              <a href="#" class="red-text link-quitar" data-id="{$this->productoItem->getId()}">Quitar</a>
            </div>
            <div class="card-action">
              <span></span>
              <button class="waves-effect waves-light btn btn-sumar" data-id="{$this->productoItem->getId()}">-</button>
              <button class="waves-effect waves-light btn btn-restar" data-id="{$this->productoItem->getId()}">+</button>
            </div>
          </div>
        </div>
html;

    }

    /**
     * @param Articulos $model
     * @return Item
     * @throws \Tg\Tienda\Exception
     */
    public static function readFromModel(Articulos $model): Item
    {
        $item = new self();
        $item->id = $model->getId();
        $item->productoItem = ProductosItem::readFromDb($model->getProductoId());
        $item->cantidad = $model->getCantidad();

        return $item;
    }

    /**
     * @param int $carritoId
     * @param int $productoId
     * @return Item
     * @throws Exception
     * @throws \Tg\Tienda\Exception
     */
    public static function readFromDb(int $carritoId, int $productoId): Item
    {
        $model = Articulos::findFirst(
            'carritoID = :carritoId AND productoId = :productoId',
            [
                'carritoId' => $carritoId,
                'productoId' => $productoId,
            ]
        );
        return self::readFromModel($model);
    }

}
