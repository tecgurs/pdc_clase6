<?php
/**
 * @Filename: Exception.php
 * @Description:
 * @CreatedAt: 16/09/19 14:55
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class Exception extends \Exception
{

}
