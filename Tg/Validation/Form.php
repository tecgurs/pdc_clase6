<?php
/**
 * @Filename: Form.php
 * @Description:
 * @CreatedAt: 16/09/19 14:41
 * @Author: Roman Cisneros romancc9@gmail.com
 * Impossible only means you haven't found the solution yet.
 */

namespace Tg\Validation;


class Form
{
    /** @var array */
    private $elements = [];
    /** @var string */
    private $message;

    public function getMessage(): string
    {

    }

    public function pushElement(Element $element): Form
    {

    }

    public function validate(): bool
    {

    }
}
