<?php

use Tg\BasicCrud\Crud;
use Tg\BasicCrud\Defaults;
use Tg\BasicCrud\Exception as CrudException;
use Tg\BasicCrud\TablaProductos;

require_once 'autoload.php';

if ($_SESSION['login'] !== true) {
  header("Location: {$baseUri}/login.php");
  exit;
}

$defaults = new Defaults();

try {
    $crud = new Crud();
    if ($crud->getTipo() == 'borrar') {
      if ($crud->getResultado() == true) {
          header("Location: {$baseUri}/index.php");
          exit;
      }
      $errorMessage = $crud->getMessage();
    }

    if ($crud->getTipo() == 'editar') {
      if ($crud->getResultado() == true) {
        $defaults = $crud->getDefaults();
      } else {
        $errorMessage = $crud->getMessage();
      }
    }

    if ($crud->getTipo() == 'botonEditar') {
      if ($crud->getResultado() == true) {
        header("Location: {$baseUri}/index.php");
        exit;
      }
      $errorMessage = $crud->getMessage();
      $defaults = $crud->getDefaults();
    }
    if ($crud->getTipo() == 'crear') {
        if ($crud->getResultado() == true) {
            header("Location: {$baseUri}/index.php");
            exit;
        }
        $errorMessage = $crud->getMessage();
        $defaults = $crud->getDefaults();
    }
} catch (CrudException $e) {

}


$tabla = new TablaProductos();

// View or Template
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Basic CRUD">
    <meta name="keywords" content="Basic CRUD, PHP, JS, Materialize">

    <title>Basic CRUD</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">

</head>
<body>

<div class="container" role="main">

  <div class="row">
    <div class="col s12">
      <div class="card center">
        <div class="card-content">
          <?php
          echo $_SESSION['nombre'] . ' ';
          echo $_SESSION['email'];
          ?>
          <a href="logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col s12">
      <div class="card center">
        <div class="card-title"><?php echo $crud->generateBotonCaption(); ?></div>
      </div>
    </div>
    <?php if (isset($errorMessage)) { ?>
    <div class="col s12">
      <div class="card center">
        <div class="card-title red-text"><?php echo $errorMessage ?></div>
      </div>
    </div>
    <?php } ?>
    <form class="col s12" action="index.php" method="post" enctype="multipart/form-data">
      <input name="id" type="hidden" value="<?php echo $defaults->id; ?>">
      <div class="row">
        <div class="input-field col s12">
          <input id="nombre" name="nombre" type="text" value="<?php echo $defaults->nombre; ?>">
          <label for="nombre">Nombre</label>
        </div>
        <div class="input-field col s12 m6">
          <input id="code" name="code" type="text" value="<?php echo $defaults->code; ?>">
          <label for="code">Code</label>
        </div>
        <div class="input-field col s12 m6">
          <input id="precio" name="precio" type="text" value="<?php echo $defaults->precio; ?>">
          <label for="precio">Precio</label>
        </div>
        <div class="input-field col s12 m6" >
          <select id="unidad" name="unidad">
            <option value="kilo" <?php if ($defaults->unidad == 'kilo') echo 'selected'; ?>>Kilo</option>
            <option value="pieza" <?php if ($defaults->unidad == 'pieza') echo 'selected'; ?>>Pieza</option>
          </select>
          <label for="unidad"></label>
        </div>
<?php if ($crud->getTipo() != 'editar') { ?>
        <div class="file-field input-field col s12 m6">
          <div class="btn blue-grey">
            <span>Archivo</span>
            <input id="foto" name="foto" type="file" accept="image/jpg">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path" type="text">
          </div>
        </div>
<?php } ?>
        <div class="col s12">
          <button class="btn blue-grey" type="submit">
              <?php echo $crud->generateBotonCaption(); ?>
          </button>
        </div>
      </div>
    </form>
  </div>

  <div class="row">
    <div class="input-field col s6">
      <input id="busqueda" name="busqueda" type="text">
      <label for="busqueda">Búsqueda</label>
    </div>
    <div id="paginador" class="col s6">
      <!--ul class="pagination">
        <li class="active"><a href="#">1</a></li>
        <li class="waves-effect"><a href="#" class="page-item" data-id="2">2</a></li>
        <li class="waves-effect"><a href="#" class="page-item" data-id="3">3</a></li>
      </ul-->
    </div>
  </div>

  <div class="row">
    <div id="tabla" class="col s12">
      <?php /*echo $tabla->toHtml();*/ ?>
    </div>
  </div>

</div>

<!--  Scripts-->
<script src="js/jquery-3.3.1.js"></script>
<script src="js/materialize.js"></script>
<script src="js/prototypes.js"></script>
<script src="js/front.js"></script>
<script>

    $(document).ready(function () {
        front.construct("<?php echo $baseUri; ?>");
    });

</script>

</body>
</html>
