<?php

require_once 'autoload.php';

if ($_SESSION['login'] !== true) {
    header("Location: {$baseUri}/login.php");
    exit;
}

$_SESSION['login'] = false;
header("Location: {$baseUri}/login.php");
exit;
