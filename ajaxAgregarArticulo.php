<?php

use Tg\Tienda\Carritos\Handler;
use Tg\Tienda\Exception as TiendaException;

require_once 'autoload.php';

$handler = new Handler();
try {
    $response = $handler->agregarArticulo();
    echo json_encode($response->toArray());
} catch (TiendaException $e) {
    switch ($e->getCode()) {
        case 1001:
            $respuesta = 'Debes enviar un productoId válido (entero positivo)';
            break;
        default: $respuesta = 'No se pudo encontrar el producto';
    }
    echo $respuesta; exit;
}

